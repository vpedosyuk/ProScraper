# ProScraper v0.2

**ProScraper** allows you to retrieve, parse and add data from [Houzz.com](https://houzz.com) and [Zillow.com](https://zillow.com) into a defined output type.

What can we scrape? Here is the list:

* Company Name
* Name of a professional
* Phone
* Address
* City
* Zip-code
* State
* Country
* Industry
* Website
* Emails

Where can we add it to?

* Google Sheets
* PostgreSQL DB
* A local CSV file

The program is also Heroku-ready, so you can easily deploy it to your Heroku instance.

If you want, you can check out [changes log](docs/CHANGES.md).

## Installing

Clone the project on your computer:

```
git clone -b v0.2 https://gitlab.com/vpedosyuk/ProScraper.git
```
Create a virtual environment there with the Python 3 interpreter:

```
virtualenv -p python3 ProScraper
cd ProScraper/
```

Activate this environment:

```
source bin/activate
```

Install all Python dependencies:

```
pip3 install -r requirements.txt
```
That's it! We've set it up.
## Basic Usage
In order to start, it's required to configure [config.json](docs/CONFIGURATION.md)

The usage format looks like the following:

```
usage: scrape.py [-h] [-w WORKER] {houzz,zillow} ...

ProScraper

positional arguments:
  {houzz,zillow}

optional arguments:
  -h, --help            show this help message and exit
  -w WORKER, --worker WORKER
```
When you scrape from Zillow:
```
usage: scrape.py zillow [-h] -c CITY -o {google_sheet,postgres,csv}
                        [-p {home-improvement-reviews,real-estate-agent-reviews,property-manager-reviews,home-builder-reviews}]

optional arguments:
  -h, --help            show this help message and exit
  -c CITY, --city CITY  Examples: San-Francisco, 290087, Boston, New-
                        Hampshire, NY
  -o {google_sheet,postgres,csv}, --output {google_sheet,postgres,csv}
  -p {home-improvement-reviews,real-estate-agent-reviews,property-manager-reviews,home-builder-reviews}, --profession {home-improvement-reviews,real-estate-agent-reviews,property-manager-reviews,home-builder-reviews}
```
When you scrape from Houzz:
```
usage: scrape.py houzz [-h] -c CITY -o {google_sheet,postgres,csv}
                       [-m {10,25,50,100}]
                       [-p {architect,design-build,general-contractor,home-builders,interior-designer,kitchen-and-bath,kitchen-and-bath-remodeling,landscape-architect,landscape-contractors,stone-pavers-and-concrete,tile-stone-and-countertop,pools-and-spas,roofing-and-gutter,solar-energy-contractors,electrical-contractors,plumbing-contractors}]

optional arguments:
  -h, --help            show this help message and exit
  -c CITY, --city CITY  Examples: San-Francisco, 290087, Boston, New-
                        Hampshire, NY
  -o {google_sheet,postgres,csv}, --output {google_sheet,postgres,csv}
  -m {10,25,50,100}, --miles {10,25,50,100}
                        Search within X miles
  -p {architect,design-build,general-contractor,home-builders,interior-designer,kitchen-and-bath,kitchen-and-bath-remodeling,landscape-architect,landscape-contractors,stone-pavers-and-concrete,tile-stone-and-countertop,pools-and-spas,roofing-and-gutter,solar-energy-contractors,electrical-contractors,plumbing-contractors}, --profession {architect,design-build,general-contractor,home-builders,interior-designer,kitchen-and-bath,kitchen-and-bath-remodeling,landscape-architect,landscape-contractors,stone-pavers-and-concrete,tile-stone-and-countertop,pools-and-spas,roofing-and-gutter,solar-energy-contractors,electrical-contractors,plumbing-contractors}
```
There is a possibility to run several workers ([Manager Guide](docs/MANAGER.md)):
```
usage: manager.py [-h] -w WORKERS [-s SECONDS]

optional arguments:
  -h, --help            show this help message and exit
  -w WORKERS, --workers WORKERS
                        How many workers will be running (don't set more than
                        15)
  -s SECONDS, --seconds SECONDS
                        How often Manager will be checking your 'Order sheet'
```
You don't need to create any *header/table/csv-file* in order to get it working. The **ProScraper** will check and overwrite them automatically.
### City
The option is ```--city=``` or ```-c``` and it is **required**.
You can type in any city, state, country or zipcode since the Scraper just transfers this value to [Houzz.com](https://houzz.com). However, in order to use it properly, it's needed to use ```-``` sign to replace spaces.
Examples:
```San-Francisco```, ```290087```, ```Boston```, ```New-Hampshire```, ```NY```, ```Kryviy-Rih```, ```Ukraine```.
### Output
The option is ```--output=``` or ```-o``` and it is **required**. So far, you can select one of the following output types:
```postgres```, ```google_sheet```, ```csv```
### Profession
The option is ```--profession=``` or ```-p``` and it is **required**. You can use any value you've configured in the appropriate section of ```config.json``` file.
### Miles (only for Houzz)
The option is ```--miles=``` or ```-m``` and it is **optional**. Default is ```10```. This option allows to define within how many miles search professionals. You can select one of the following: ```10```,```25```,```50```,```100```.
### Examples
```
./scrape.py houzz -c Kiev -o postgres -p architect -m 100
```
```
./scrape.py zillow --city=Los-Angeles -o google_sheet -p real-estate-agent-reviews
```