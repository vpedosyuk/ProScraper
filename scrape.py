#!/usr/bin/env python3

__author__ = 'Vladyslav Pedosyuk'

import json
import argparse
from core import Houzz
from core import Zillow


def select_scraper(func):
    def wrapper():
        init_scraper_params = func()
        if init_scraper_params['parsed_arguments'].input == 'houzz':
            return Houzz(**init_scraper_params)
        elif init_scraper_params['parsed_arguments'].input == 'zillow':
            return Zillow(**init_scraper_params)
    return wrapper


@select_scraper
def init():
    # load the config file for checking all parameters we need in a parser (i.e. 'professions' list)
    # and further passing it to a Scraper class
    params = json.load(open('config/config.json'))

    # parse input arguments
    parser = argparse.ArgumentParser(description='ProScraper', formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-w', '--worker', default=0, type=int, required=False)

    # we can define any set of parameters for each input type like Houzz or Zillow
    subparsers = parser.add_subparsers(dest='input')

    # a set of params for Houzz
    parser_houzz = subparsers.add_parser('houzz')
    parser_houzz.add_argument('-c', '--city', help='Examples: San-Francisco, 290087, Boston, New-Hampshire, NY', required=True)
    parser_houzz.add_argument('-o', '--output', choices=['google_sheet', 'postgres', 'csv'], required=True, type=str)
    parser_houzz.add_argument('-m', '--miles', help='Search within X miles', choices=[10, 25, 50, 100], default=10, type=int)
    parser_houzz.add_argument('-p', '--profession', choices=params['professions']['houzz'], default='architect', type=str)

    # a set of params for Zillow
    parser_zillow = subparsers.add_parser('zillow')
    parser_zillow.add_argument('-c', '--city', help='Examples: San-Francisco, 290087, Boston, New-Hampshire, NY', required=True)
    parser_zillow.add_argument('-o', '--output', choices=['google_sheet', 'postgres', 'csv'], required=True, type=str)
    parser_zillow.add_argument('-p', '--profession', choices=params['professions']['zillow'], default='real-estate-agent-reviews', type=str)

    return {
        'parsed_arguments': parser.parse_args(),
        'file_config': params
    }


def main():
    scraper = None
    try:
        scraper = init()
        scraper.start()
        scraper.stop()
    except KeyboardInterrupt:
        if scraper:
            scraper.stop()
        print('\nManually interrupted. Exiting.')


if __name__ == '__main__':
    main()
