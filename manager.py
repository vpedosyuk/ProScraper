#!/usr/bin/env python3

import json
import time
import socket
import argparse
import pygsheets
from worker import Worker


class Manager(object):

    def __init__(self, workers):
        self.workers = workers
        self.sheet = get_control_sheet()

        # initialization of the workers pool
        self.worker_processes = {}
        for work_num in range(1, self.workers + 1):
            self.worker_processes.update({work_num: None})

    def start(self):
        try:
            orders_list = get_orders(self.sheet)
        except pygsheets.RequestError:
            time.sleep(5)
            orders_list = get_orders(self.sheet)

        for work_num in range(1, self.workers + 1):
            if self.worker_processes[work_num] is None:
                if orders_list:
                    order = orders_list.pop(0)
                    wk = Worker(input=order[1], output=order[2], worker_row=order[0].row, worker=work_num, city=order[3], profession=order[4], miles=order[5])
                    self.worker_processes[work_num] = wk
                    self.worker_processes[work_num].start()
                    # filling info about the worker into the gsheet
                    worker_row = self.worker_processes[work_num].parameters['worker_row']
                    self.sheet.update_cell((worker_row, 7), 'in process')
                    self.sheet.update_cell((worker_row, 8), socket.gethostname())
                    self.sheet.update_cell((worker_row, 9), 'Worker %d' % work_num)
                    self.sheet.update_cell((worker_row, 10), self.worker_processes[work_num].pid)

    def check_status(self):
        for work_num in range(1, self.workers + 1):
            # worker is empty
            if self.worker_processes[work_num] is None:
                continue

            # finished successfully
            elif self.worker_processes[work_num].status() != 0:
                self.sheet.update_cell((self.worker_processes[work_num].parameters['worker_row'], 7), 'finished')
                # cleaning up the worker
                self.worker_processes[work_num] = None

            # still running
            elif self.worker_processes[work_num].status() == 0:
                continue

        if None in self.worker_processes.values():
            # there is at least one empty worker
            return True
        else:
            return False

    def stop(self):
        for each in self.worker_processes.values():
            if each is Worker:
                each.stop()


def get_control_sheet():
    params = json.load(open('config/config.json'))
    client = pygsheets.authorize(service_file=params['google_sheet']['service_key'])
    spreadsheet = client.open(params['google_sheet']['control_spreadsheet']['name'])
    return spreadsheet.worksheet(property='title', value=params['google_sheet']['control_spreadsheet']['worksheet_title'])


def get_orders(sheet):
    yes_list = sheet.get_col(6, returnas='cell', include_empty=False)
    list_of_needed_rows = []
    for cell in yes_list:
        if cell.value == 'yes':
            if cell.neighbour('right').value != 'in process':
                if cell.neighbour('right').value != 'finished':
                    row = sheet.get_row(cell.row, returnas='matrix', include_empty=True)
                    # we also need to insert the cell object into the list of command parameters
                    # in order to be able to get an appropriate column number of the worker on the gsheet
                    row.insert(0, cell)
                    list_of_needed_rows.append(row)
    return list_of_needed_rows


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-w', '--workers', required=True, type=int, help='How many workers will be running (don\'t set more than 15)')
    parser.add_argument('-s', '--seconds', required=False, type=int, help='How often Manager will be checking your \'Order sheet\'', default=20)
    args = parser.parse_args()

    manager = Manager(args.workers)

    try:
        status_of_workers = True
        while 1:
            if status_of_workers is True:
                manager.start()
            time.sleep(args.seconds)
            status_of_workers = manager.check_status()
    except KeyboardInterrupt:
        manager.stop()


if __name__ == '__main__':
    main()
