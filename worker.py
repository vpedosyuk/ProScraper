#!/usr/bin/env python3

import os
import re
import subprocess


class Worker(object):

    def __init__(self, input, city, worker, output, worker_row, profession, miles=None):
        self.parameters = {
            'input': input,
            'city': city,
            'worker': worker,
            'output': output,
            'profession': profession,
            'worker_row': worker_row
        }
        if miles:
            self.parameters['miles'] = miles
        self.process = None
        self.pid = None

    def start(self):
        command = ''
        if self.parameters['input'] == 'houzz':
            command = re.split(' ', 'python scrape.py -w {worker} {input} -c {city} -p {profession} -o {output} -m {miles}'.format(**self.parameters))
        elif self.parameters['input'] == 'zillow':
            command = re.split(' ', 'python scrape.py -w {worker} {input} -c {city} -p {profession} -o {output}'.format(**self.parameters))
        self.process = subprocess.Popen(command)
        self.pid = self.process.pid

    def status(self):
        status = self.process.poll()
        if status is None:
            return 0
        elif status == 0:
            return os.getpid()

    def stop(self):
        try:
            for i in range(5):
                os.kill(self.pid, 15)
        except OSError:
            pass
