#!/usr/bin/env python3

import requests
from .scraper import Scraper
from bs4 import BeautifulSoup


class Zillow(Scraper):
    __name__ = 'Zillow'

    def get_company(self, bs4_page):
        try:
            business_name_tag = bs4_page.find('dd', attrs={'class': 'zsg-lg-3-5 profile-information-address'}).strings
            # zero element is a company name
            business_name = [repr(text) for text in business_name_tag][0]
        except AttributeError:
            return None
        if business_name:
            return business_name.replace('\'', '')
        else:
            return None

    def get_name(self, bs4_page):
        """
            The function returns a dictionary with a first name and a second name. If there is no
            either first or second name, this part will be returned as None.
            :param bs4_page: a BeautifulSoup object
            :return dict: a dictionary like
                    {
                        'first': 'Vladyslav',
                        'last': 'Pedosyuk'
                    }
        """
        names = {
            "first": None,
            "last": None
        }
        contact_info = bs4_page.find(class_='ctcd-user-name')
        if contact_info:
            full_name = str(contact_info.text)
            # get 'first name' and 'last name' by splitting 'contact' field
            full_name_list = full_name.split(' ')
            if len(full_name_list) >= 2:
                names['first'] = full_name_list[0]
                names['last'] = full_name_list[1]
                return names
            else:
                names['first'] = full_name_list[0]
                return names
        else:
            return names

    def get_phone(self, bs4_page):
        phone = None
        phone_tag = bs4_page.find(class_='zsg-lg-3-5 profile-information-mobile')
        if phone_tag:
            phone = phone_tag.text
        else:
            # phone can also be stored in a different tag
            phone_tag = bs4_page.find(class_='zsg-lg-3-5 profile-information-cell')
            if phone_tag:
                phone = phone_tag.text
        return phone

    def get_address(self, bs4_page):
        """
            The function returns a dictionary with a line 1 and a line 2 of the address. If there is no
            either first or second name or both, this part will be returned as None.
            :param bs4_page: a BeautifulSoup object
            :return dict: a dictionary like
                    {
                        'ln1': 'Vladyslav',
                        'ln2': 'Pedosyuk'
                    }
        """
        address = {
            'ln1': None,
            'ln2': None
        }
        try:
            address_full = bs4_page.find(class_='street-address').text
        except AttributeError:
            return address
        if address_full:
            # get 'ln1' and 'ln2' by splitting ',' or '#', or 'suite', or 'ste' delimiter
            for delimiter in [',', 'ste', 'suite', '#']:
                if delimiter in address_full:
                    address_full_split = address_full.split(delimiter)
                    address['ln1'] = address_full_split[0]
                    address['ln2'] = address_full_split[1]
                    return address
                else:
                    address['ln1'] = address_full
                    return address
        else:
            return address

    def get_website(self, bs4_page):
        try:
            website_tag = bs4_page.find(class_='zsg-lg-3-5 profile-information-websites').contents
            if website_tag:
                return website_tag[0]['href']
        except AttributeError:
            return None

    def get_city(self, bs4_page):
        try:
            return bs4_page.find(class_='locality').text
        except AttributeError:
            return None

    def get_zipcode(self, bs4_page):
        try:
            zipcode = bs4_page.find(class_='postal-code').text
            # clean zipcode
            try:
                return zipcode.split(' ')[1]
            except IndexError:
                return None
        except AttributeError:
            return None

    def get_industry(self, bs4_page):
        # we just use our input profession type but a bit decorated
        splitted_industry = self._userconf.profession.split('-')
        return ' '.join([word.capitalize() for word in splitted_industry])

    def get_country(self, bs4_page):
        return 'USA'

    def get_links(self):
        """
        Checking every page we can generate on valid links until we reach 'ldb-no-results-inner' class. It's important to
        initiate an output instance before executing this function since it uses get_unique_items() of the output.
        :return list: a list of professionals links without duplicates
        """

        page_number = 1
        links = []
        self._logger.info('Fetching all possible links from zillow.com...')

        # a simple dynamic counter for checked pages
        counter = 0

        while True:
            counter += 1
            url = '{0}/{1}/{2}/?page={3}'.format('https://www.zillow.com', self._userconf.city, self._userconf.profession, page_number)

            # let's use a user-agent to avoid reCAPTCHA 2.0
            user_agent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36'
            language = 'en-US'

            headers = {
                "Accept-Language": language,
                "User-Agent": user_agent
            }

            proxies = {
                'https': 'http://767emvylsp5j9s:-tekwp5upHN3mBka_tssBCSkcQ@us-east-static-03.quotaguard.com:9293'
            }

            s = requests.session()
            r = s.get(url, headers=headers, proxies=proxies)
            r.encoding = 'utf-8'

            page = BeautifulSoup(r.text, 'lxml')

            if page.find(class_='ldb-no-results-inner') or page.find(class_='captcha-container'):
                break
            else:
                for tag in page.find_all(class_='ldb-contact-name ldb-font-bold'):
                    profile = tag.find('a')['href']
                    if profile:
                        links.append('{}{}'.format('https://www.zillow.com', profile))

            print('Fetched {} links, checked {} pages.'.format(len(links), counter), end='\r')
            page_number += 1

        self._logger.info('Result: fetched {} links, checked {} pages.'.format(len(links), counter))

        # generating a list of links here, which won't contain records from the 'output' instance
        # i.e. removing duplicates
        self._logger.debug('\nRemoving duplicates...')

        items_in_output = self._output.get_unique_items()
        result = [each for each in set(links) if each not in items_in_output]

        self._logger.info('Removed {} duplicates.\n'.format(len(links) - len(result)))

        return result
