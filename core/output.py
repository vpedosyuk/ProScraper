#!/usr/bin/env python3

from .tools import log_config


class Output(object):
    """
    The Output class represents an abstract class of different outputs that
    communicate with the Scraper class
    """
    __name__ = 'Output'
    _unique_column = 'link'
    _ready = False
    _logger = None

    def __init__(self, dict_config):
        self._logger = log_config(self.__name__, dict_config['log'])
        self.init_check(dict_config)

    def ready(self):
        return self._ready

    def get_unique_items(self):
        pass

    def close(self):
        pass

    def append(self, data_object):
        pass

    # the function must be overwritten in a subclass
    def init_check(self, dict_config):
        return None
