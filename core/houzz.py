#!/usr/bin/env python3

import re
import requests
from .scraper import Scraper
from bs4 import BeautifulSoup


class Houzz(Scraper):
    __name__ = 'Houzz'

    def get_company(self, bs4_page):
        try:
            business_name = bs4_page.find(class_='profile-full-name')
        except AttributeError:
            return None
        if business_name:
            return business_name.text
        else:
            return None

    def get_name(self, bs4_page):
        """
            The function returns a dictionary with a first name and a second name. If there is no
            either first or second name, this part will be returned as None.
            :param bs4_page: a BeautifulSoup object
            :return dict: a dictionary like
                    {
                        'first': 'Vladyslav',
                        'last': 'Pedosyuk'
                    }
        """
        names = {
            "first": None,
            "last": None
        }
        contact_info = bs4_page.find_all(class_='info-list-text')
        # check if there is 'contact' field
        contact_field = bs4_page.find(class_='hzi-font hzi-Man-Outline')
        if contact_field:
            try:
                full_name = str(contact_info[1].text)
            except IndexError:
                full_name = None
            # get 'first name' and 'last name' by splitting 'contact' field
            if full_name:
                # cut off 'Contact:' word and split first and last names
                full_name_list = re.search(r'.*: (.*)', full_name).group(1).split(' ')
                # check if there is also 'last name'
                if len(full_name_list) >= 2:
                    names['first'] = full_name_list[0].title()
                    names['last'] = full_name_list[1].title()
                    return names
                else:
                    names['first'] = full_name_list[0].title()
                    return names
        else:
            return names

    def get_phone(self, bs4_page):
        # most likely a phone number is here
        try:
            possible_phone_1 = bs4_page.find(class_='click-to-call-link text-gray-light trackMe')
        except AttributeError:
            possible_phone_1 = None
        if possible_phone_1:
            real_phone = possible_phone_1['phone']
        else:
            # however, it could be also here
            try:
                possible_phone_2 = bs4_page.find(class_='set_needed_profession-contact-text')
            except AttributeError:
                possible_phone_2 = None
            if possible_phone_2:
                real_phone = possible_phone_2.contents[0]
            else:
                real_phone = None

        # checking on '+' and 'Website' existing in the number
        if real_phone:
            if '+' in real_phone:
                return real_phone[1:]
            elif 'Website' in real_phone:
                return None
            else:
                return real_phone
        else:
            return None

    def get_address(self, bs4_page):
        """
            The function returns a dictionary with a line 1 and a line 2 of the address. If there is no
            either first or second name or both, this part will be returned as None.
            :param bs4_page: a BeautifulSoup object
            :return dict: a dictionary like
                    {
                        'ln1': 'Vladyslav',
                        'ln2': 'Pedosyuk'
                    }
        """
        address = {
            'ln1': None,
            'ln2': None
        }
        try:
            address_fields = bs4_page.find_all('span', {'itemprop': 'streetAddress'})
        except AttributeError:
            return address
        if address_fields:
            # unite all fields with 'space' delimiter into 'address_full'
            address_full = ' '.join([tag.text for tag in address_fields])
            # get 'ln1' and 'ln2' by splitting ',' or '#', or 'suite', or 'ste' delimiter
            for delimiter in [',', 'ste', 'suite', '#']:
                if delimiter in address_full:
                    address_full_split = address_full.split(delimiter)
                    address['ln1'] = address_full_split[0]
                    address['ln2'] = address_full_split[1]
                    return address
                else:
                    address['ln1'] = address_full
                    return address
        else:
            return address

    def get_city(self, bs4_page):
        try:
            return bs4_page.find('span', {'itemprop': 'addressLocality'}).text
        except AttributeError:
            return None

    def get_zipcode(self, bs4_page):
        try:
            return bs4_page.find('span', {'itemprop': 'postalCode'}).text
        except AttributeError:
            return None

    def get_state(self, bs4_page):
        try:
            return bs4_page.find('span', {'itemprop': 'addressRegion'}).text
        except AttributeError:
            # if there is no 'addressRegion'on the page, we can try to parse it from links
            state_urls = bs4_page.find_all('a', {'itemprop': 'url'})
            if state_urls:
                for url in state_urls:
                    # after '--' is usually located a state or country abbreviation like UA or WI
                    if '--' in url['href']:
                        parts_of_url = re.split('--', url['href'])
                        if len(parts_of_url) > 1:
                            return parts_of_url[1]
                        else:
                            return None

    def get_country(self, bs4_page):
        try:
            return bs4_page.find('span', {'itemprop': 'addressCountry'}).text
        except AttributeError:
            return None

    def get_industry(self, bs4_page):
        try:
            return bs4_page.find_all('span', {'itemprop': 'title'})[1].text
        except IndexError:
            return None

    def get_website(self, bs4_page):
        try:
            return bs4_page.find('a', {'compid': 'Profile_Website'})['href']
        except TypeError:
            return None

    def get_links(self):
        """
        Checking every page we can generate on valid links until we reach 'noResult' tag. It's important to
        initiate an output instance before executing this function since it uses get_unique_items() of the output.
        :return list: a list of professionals links without duplicates
        """

        page_number = 0
        links = []
        # print('Fetching all possible links from houzz.com...')
        self._logger.info('Fetching all possible links from houzz.com...')

        # a simple dynamic counter for checked pages
        counter = 0
        last_page = False

        while True:
            counter += 1
            url = '{0}/{1}/c/{2}/d/{3}/p/{4}'.format('https://www.houzz.com/professionals',
                                                     self._userconf.profession,
                                                     self._userconf.city,
                                                     self._userconf.miles,
                                                     page_number)

            user_agent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36'
            language = 'en-US'

            headers = {
                "Accept-Language": language,
                "User-Agent": user_agent
            }

            proxies = {
                'https': 'http://767emvylsp5j9s:-tekwp5upHN3mBka_tssBCSkcQ@us-east-static-03.quotaguard.com:9293'
            }

            s = requests.session()
            r = s.get(url, headers=headers, proxies=proxies)
            r.encoding = 'utf-8'

            page = BeautifulSoup(r.text, 'lxml')

            if page.find(class_='noResult') or last_page:
                break
            else:
                for tag in page.find_all(class_='pro-title'):
                    link = re.match('https.*pro.*', tag['href'])
                    if link:
                        links.append(link.group(0))
                    elif not page.find(class_='navigation-button next'):
                        last_page = True

            # print('Fetched {} links, checked {} pages.'.format(len(links), counter), end='\r')
            page_number += 15

        self._logger.info('Result: fetched {} links, checked {} pages.'.format(len(links), counter))

        # generating a list of links here, which won't contain records from the 'output' instance
        # i.e. removing duplicates

        # print('\n\nRemoving duplicates...')
        self._logger.debug('\nRemoving duplicates...')

        items_in_output = self._output.get_unique_items()
        result = [each for each in set(links) if each not in items_in_output]

        # print('Removed {} duplicates.\n'.format(len(links) - len(result)))
        self._logger.info('Removed {} duplicates.\n'.format(len(links) - len(result)))

        return result
