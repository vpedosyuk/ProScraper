#!/usr/bin/env python3

import json
import logging
import sendgrid
from intercom.client import Client
from logentries import LogentriesHandler


def log_config(name, logconf):
    formatter = logging.Formatter('[{asctime}] [{name}] [{levelname}]\n{message}\n', style='{')
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    # Stream handler
    if logconf['stream']['enabled'] == 'yes':
        stream_handler = logging.StreamHandler()
        stream_formatter = logging.Formatter('{message}', style='{')
        stream_handler.setFormatter(stream_formatter)
        stream_level = logging.getLevelName(logconf['stream']['level'].upper())
        stream_handler.setLevel(stream_level)
        logger.addHandler(stream_handler)

    # File handler
    if logconf['file']['enabled'] == 'yes':
        file_handler = logging.FileHandler(logconf['file']['name'])
        file_handler.setFormatter(formatter)
        file_level = logging.getLevelName(logconf['file']['level'].upper())
        file_handler.setLevel(file_level)
        logger.addHandler(file_handler)

    # Logentries handler
    if logconf['logentries']['enabled'] == 'yes':
        logentries_handler = LogentriesHandler(logconf['logentries']['token'])
        logentries_formatter = logging.Formatter('[{levelname}] [{name}] {message}', style='{')
        logentries_handler.setFormatter(logentries_formatter)
        logentries_level = logging.getLevelName(logconf['logentries']['level'].upper())
        logentries_handler.setLevel(logentries_level)
        logger.addHandler(logentries_handler)

    return logger


def sendgrid_email(token, input_email, input_data, input_class):
    def push_email(sg, list):
        professional = sg.client.contactdb.recipients.post(request_body=data)
        js_body = json.loads(professional.body)
        try:
            recipient_id = js_body['persisted_recipients'][0]
            response = sg.client.contactdb.lists._(list).recipients._(recipient_id).post()
            return response.status_code
        except IndexError:
            return js_body['errors']

    def get_list_id(sg):
        # check if there is any list with name of the input class
        lists = sg.client.contactdb.lists.get()
        if lists:
            for each in json.loads(lists.body)['lists']:
                if each['name'] == input_class:
                    return each['id']
        return None

    def create_list(sg, name):
        data = {
            "name": name
        }
        created_list = sg.client.contactdb.lists.post(request_body=data)
        return json.loads(created_list.body)['id']

    def get_segment_id(sg):
        segments = sg.client.contactdb.segments.get()
        if segments:
            for each in json.loads(segments.body)['segments']:
                if each['name'] == input_data['industry']:
                    return each['id']
        return None

    def create_segment(sg, list_id):
        data = {
            "conditions": [
                {
                    "and_or": "",
                    "field": "industry",
                    "operator": "eq",
                    "value": input_data['industry']
                }
            ],
            "list_id": list_id,
            "name": input_data['industry']
        }
        created_segment = sg.client.contactdb.segments.post(request_body=data)
        return created_segment.status_code

    def check_custom_fields(sg, input_fields):
        # delete reserved fields that are not actually 'custom' from the input list
        input_fields = set(input_fields) - {'first_name', 'email', 'last_name'}

        response = json.loads(sg.client.contactdb.custom_fields.get().body)
        retrieved_fields = [fl['name'] for fl in response['custom_fields']]
        non_ex_fields = input_fields - set(retrieved_fields)

        return non_ex_fields

    def create_custom_field(sg, field_to_add):
        field = {
            "name": field_to_add,
            "type": "text"
        }
        sg.client.contactdb.custom_fields.post(request_body=field)

    data = [
        {
            "email": input_email,
            "first_name": input_data['first_name'],
            "last_name": input_data['last_name'],
            "phone": input_data['phone'],
            "industry": input_data['industry'],
            "full_name": "{} {}".format(input_data['first_name'], input_data['last_name']),
            "address_line_1": input_data['address_ln1'],
            "address_line_2": input_data['address_ln2'],
            "address_city": input_data['city'],
            "address_state": input_data['state'],
            "company_name": input_data['company'],
            "address_zip": input_data['zipcode'],
            "address_country": input_data['country'],
            "website": input_data['website'],
            "lead_link": input_data['link']
        }
    ]

    # get connection instance
    conn = sendgrid.SendGridAPIClient(apikey=token)

    # check custom fields
    non_ex_cust_fields = check_custom_fields(conn, data[0].keys())
    if non_ex_cust_fields:
        for each in non_ex_cust_fields:
            create_custom_field(conn, each)
    # check if the list already exists on SendGrid
    list_id = get_list_id(conn)
    if list_id:
        # check if the segment already exists in the list
        if not get_segment_id(conn):
            create_segment(conn, list_id)
    else:
        list_id = create_list(conn, input_class)
        create_segment(conn, list_id)

    return push_email(conn, list_id)


def intercom(token, input_email, input_data):
        if not input_data['first_name']:
            return 'Skipped'

        ic = Client(personal_access_token=token)

        attributes = {
            'Industry': input_data['industry'],
            'Company Name': input_data['company'],
            'Address Line 1': input_data['address_ln1'],
            'Address Line 2': input_data['address_ln2'],
            'City': input_data['city'],
            'State': input_data['state'],
            'Zip': input_data['zipcode'],
            'Country': input_data['country'],
            'Website': input_data['website']
        }

        full_name = '{} {}'.format(input_data['first_name'], input_data['last_name'])

        try:
            ic.leads.create(email=input_email,
                                  phone=input_data['phone'],
                                  name=full_name,
                                  custom_attributes=attributes)
            return 'Success'
        except:
            return 'Failed'
