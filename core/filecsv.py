#!/usr/bin/env python3

import os
import csv
from .output import Output


class FileCSV(Output):
    __name__ = 'FileCSV'
    _file_read = None
    _file_write = None
    _csv_reader = None
    _csv_writer = None

    def get_unique_items(self):
        return [row[self._unique_column] for row in self._csv_reader]

    def close(self):
        self._file_read.close()
        self._file_write.close()

    def init_check(self, dict_config):
        # print('Checking the CSV file...\n')
        self._logger.info('Checking the CSV file...\n')

        # predefined list of columns
        fieldnames = ['link', 'company', 'first_name', 'last_name', 'phone', 'address_ln1', 'address_ln2',
                      'city', 'zipcode', 'state', 'country', 'industry', 'website', 'emails']

        try:
            # the file opened in 'read' mode and a reader object
            try:
                self._file_read = open('{}.csv'.format(dict_config['csv']['name']), newline='', encoding='utf-8')
            except FileNotFoundError:
                open('{}.csv'.format(dict_config['csv']['name']), 'a', newline='', encoding='utf-8')
                self._file_read = open('{}.csv'.format(dict_config['csv']['name']), newline='', encoding='utf-8')

            self._csv_reader = csv.DictReader(self._file_read, fieldnames=fieldnames)

            # check whether file is empty or not
            if os.stat('{}.csv'.format(dict_config['csv']['name'])).st_size:
                # if it's not empty, check header

                # print('File is not empty.')
                self._logger.info('File is not empty.')

                header = next(csv.reader(self._file_read))
            else:
                # print('File is empty.')
                self._logger.info('File is empty.')

                header = None
            # check the first line from the file
            if header:
                # print('Checking existing header...')
                self._logger.info('Checking existing header...')

                for index in range(0, len(header)):
                    if header[index] == fieldnames[index]:
                        self._ready = True
                        continue
                    else:
                        self._ready = False
                        break
            else:
                # the file is empty, we can write the header
                self._ready = False

            # the file opened in 'append' mode and a writer object
            self._file_write = open('{}.csv'.format(dict_config['csv']['name']), 'a', newline='', encoding='utf-8')
            self._csv_writer = csv.DictWriter(self._file_write, fieldnames=fieldnames)
            if self._ready:
                # print('Header is OK')
                self._logger.info('Header is OK')
            else:
                self._csv_writer.writeheader()
                self._logger.info('Header in the file: {}\nNew header: {}'.format(header, fieldnames))
                # print('Wrote a new header.')
            self._ready = True

            # print('The file config is OK.\n')
            self._logger.info('The file config is OK.\n')

        except (csv.Error, PermissionError) as error:
            # print('Critical error. Please check log for details.')
            self._logger.critical(error)

            self._ready = False

    def append(self, data_dict):
        self._csv_writer.writerow(data_dict)
