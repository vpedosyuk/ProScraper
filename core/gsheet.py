#!/usr/bin/env python3

import pygsheets
from .output import Output


class GoogleSheet(Output):
    __name__ = 'GoogleSheet'
    _sheet = None
    _header = None

    # predefined list of columns
    fieldnames = ['link', 'company', 'first_name', 'last_name', 'phone', 'address_ln1', 'address_ln2',
                  'city', 'zipcode', 'state', 'country', 'industry', 'website', 'emails']

    # We need to handle a length of unique items for using in append()
    # It's needed to use such a solution in order to avoid get_unique_items() call after every append() call
    # It allows us to significantly decrease a general amount of Google API calls
    unique_items_length = None

    def get_unique_items(self):
        # get an index of self._unique_column
        index = None
        for key, value in self._header.items():
            if value == self._unique_column:
                index = key
                break

        # unique items
        if index:
            unique = self._sheet.get_col(index)
        else:
            unique = None

            # print('Couldn\'t find {}'.format(self._unique_column))
            self._logger.critical('Couldn\'t find {}'.format(self._unique_column))
            self._logger.critical(self._header)

        # update the length counter
        self.unique_items_length = len(unique)

        return unique

    def init_check(self, dict_config):
        # print('Checking the Google Sheet...\n')
        self._logger.info('Checking the Google Sheet...\n')

        # print('Trying to authorize using {} ...'.format(dict_config['google_sheet']['service_key']), end='\r')
        self._logger.info('Trying to authorize using {} ...'.format(dict_config['google_sheet']['service_key']), end='\r')

        try:
            client = pygsheets.authorize(service_file=dict_config['google_sheet']['service_key'])

            # print('Authorized successfully.')
            self._logger.info('Authorized successfully.')

        except pygsheets.AuthenticationError as error:
            client = None

            # print('Could not authorize. Check log for details.')
            self._logger.critical('Could not authorize.')
            self._logger.critical(error)
            self._ready = False

        if client:
            try:
                spreadsheet = client.open(dict_config['google_sheet']['out_spreadsheet']['name'])
                self._sheet = spreadsheet.worksheet(property='title', value=dict_config['google_sheet']['out_spreadsheet']['worksheet_title'])

                # print('Connected to "{}" spreadsheet, {}.'.format(dict_config['google_sheet']['out_spreadsheet']['name'], dict_config['google_sheet']['out_spreadsheet']['worksheet_title']))
                self._logger.info('Connected to "{}" spreadsheet, {}.'.format(dict_config['google_sheet']['out_spreadsheet']['name'], dict_config['google_sheet']['out_spreadsheet']['worksheet_title']))

                self._header = self.map_header()
                if not self._header:

                    # print('File is empty. Writing header.')
                    self._logger.info('File is empty. Writing header.')

                    self._header = self.create_header(self.fieldnames)

                self._ready = True

                # print('The worksheet is OK\n')
                self._logger.info('The worksheet is OK\n')

            except (pygsheets.SpreadsheetNotFound, pygsheets.WorksheetNotFound) as error:
                # print('Could not find the spreadsheet. Check log for details.')
                self._logger.critical('Could not find the spreadsheet.')
                self._logger.critical(error)
                self._ready = False

    def create_header(self, header):
        # update header in the sheet
        self._sheet.update_row(1, header)
        # and return a mapped header
        header = {key: value for key, value in enumerate(header, 1)}

        # print('Header is OK')
        self._logger.info('Header is OK')

        return header

    def map_header(self):
        self._sheet.refresh()

        # print('Checking header...')
        self._logger.info('Checking header...')

        first_row = self._sheet.get_row(1)

        mapped_header = {}

        # check if the first row is not empty
        if first_row[0]:
            # map all existing columns in the header of the worksheet using their positional index on Google Sheet
            for i in range(0, len(first_row)):
                for name in self.fieldnames:
                    if first_row[i] == name:
                        # increment the positional index because indexing on Google Sheets starts from 1
                        mapped_header[i + 1] = name
            # check whether all fieldnames exist in the current header
            if len(mapped_header) == len(self.fieldnames):

                # print('Header is OK')
                self._logger.info('Header is OK')

                return mapped_header
            else:

                # print('Incorrect header. Overwriting header.')
                self._logger.info('Incorrect header. Overwriting header.')
                self._logger.info(first_row)

                return self.create_header(self.fieldnames)
        # if the first row is empty then return None
        else:
            return None

    def append(self, data_object):
        # reformat data_object according to the header mapping
        reformatted_data = [data_object[self._header[i]] for i in range(1, len(self._header) + 1)]

        # insert data
        self._sheet.insert_rows(self.unique_items_length, values=reformatted_data)

        self.unique_items_length += 1
