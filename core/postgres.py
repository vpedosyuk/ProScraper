#!/usr/bin/env python3

import psycopg2
import psycopg2.extras
from datetime import datetime
from .output import Output


class Postgres(Output):
    __name__ = 'Postgres'
    _unique_column = 'lead_source'
    _connection = None
    _table_name = 'marketing_list'

    def init_check(self, dict_config):
        # print('Checking the Postgres instance...')
        self._logger.info('Checking the Postgres instance...')

        try:
            self._connection = psycopg2.connect(**dict_config['postgres'])

            # print('Connected to the Postgres instance successfully.')
            self._logger.info('Connected to the Postgres instance successfully.')

            self.init_db()
            self._ready = True

            # print('Postgres config is OK.\n')
            self._logger.info('Postgres config is OK.\n')

        except psycopg2.OperationalError as error:
            self._ready = False

            # print("Faced critical error, check log for details.")
            self._logger.critical(error)

    def init_db(self):
        # boolean result
        if_inited = False

        cursor = self._connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # define a table structure
        defined_struct = {
            self._unique_column:    'text',
            "company_name":         'text',
            "first_name":           'text',
            "last_name":            'text',
            "phone":                'text',
            "addr_ln1":             'text',
            "addr_ln2":             'text',
            "city":                 'text',
            "zip":                  'text',
            "state":                'text',
            "country":              'text',
            "industry":             'text',
            "website":              'text',
            "email_address":        'text',
            "cr_time":              'text'
        }

        # get a list of fields in the table
        q = """                              
        SELECT column_name as name, data_type as type
        FROM information_schema.columns
        WHERE table_name = '{}';
        """.format(self._table_name)

        cursor.execute(q)
        real_struct = cursor.fetchall()

        # check fields types and names with the defined table structure
        if real_struct:
            for row in real_struct:
                if row['name'] in defined_struct and row['type'] == defined_struct[row['name']]:
                    if_inited = True
                else:

                    # print('The "{}" table already exists and has an incorrect format.\n'
                    #      'Please change the table name in config.json or modify the current table.\n'.format(self._table_name))

                    self._logger.critical('The "{}" table already exists and has an incorrect format. Please change the table name in config.json or modify the current table.\n'.format(self._table_name))

                    if_inited = False
                    break
        else:

            # print("Creating a houzz table...")
            self._logger.debug("Creating a marketing_list table...")

            # init db
            q = """
            CREATE TABLE {table} (
            {unique_column} {unique_type} UNIQUE, 
            company_name    {company},
            first_name      {first_name},
            last_name       {last_name},
            phone           {phone},
            addr_ln1        {address_ln1},
            addr_ln2        {address_ln2},
            city            {city},
            zip             {zipcode},
            state           {state},
            country         {country},
            industry        {industry},
            website         {website},
            email_address   {emails},
            cr_time         {cr_time}
            );
            """.format(table=self._table_name, unique_column=self._unique_column,
                       unique_type=defined_struct[self._unique_column], **defined_struct)

            cursor.execute(q)
            self._connection.commit()
            if_inited = True

            self._logger.debug("Created successfully.\n")

        cursor.close()
        return if_inited

    def get_unique_items(self):
        self._logger.debug("Getting a list of already scraped professionals...")

        cursor = self._connection.cursor()

        q = """
        SELECT {} 
        FROM {};
        """.format(self._unique_column, self._table_name)

        cursor.execute(q)
        data = cursor.fetchall()
        cursor.close()

        # print("Received.")
        self._logger.debug("Received.")

        return [tup[0] for tup in data]

    def append(self, data_dict):
        # escape single-quotes for further postgres processing
        for key in data_dict:
            if data_dict[key]:
                data_dict[key] = data_dict[key].replace(r"'", r"''")

        cursor = self._connection.cursor()
        q = """
        INSERT INTO {table}
        ({unique_col}, company_name, first_name, last_name, phone, addr_ln1, addr_ln2, city, zip,
         state, country, industry, website, email_address, cr_time)
        VALUES
        ('{link}', '{company}', '{first_name}', '{last_name}', '{phone}', '{address_ln1}', '{address_ln2}',
         '{city}', '{zipcode}', '{state}', '{country}', '{industry}', '{website}', '{emails}', '{cr_time}'); 
        """.format(table=self._table_name, unique_col=self._unique_column,
                   cr_time=datetime.now().strftime('%m-%d-%y %H:%M:%S'), **data_dict)

        cursor.execute(q)
        self._connection.commit()
        cursor.close()

    def close(self):
        if self._connection:
            self._connection.close()
