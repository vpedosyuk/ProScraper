#!/usr/bin/env python3

import re
import requests
from tqdm import tqdm
from .tools import log_config
from .tools import intercom
from .tools import sendgrid_email
from .gsheet import GoogleSheet
from .postgres import Postgres
from .filecsv import FileCSV
from bs4 import BeautifulSoup


class Scraper(object):
    __name__ = 'Scraper'
    _params = None
    _logger = None
    _userconf = None
    _output = None
    _links = None

    def __init__(self, parsed_arguments, file_config):
        self._params = file_config
        self._logger = log_config(self.__name__, self._params['log'])
        self._userconf = parsed_arguments
        self._output = self.init_output()


    def get_company(self, bs4_page):

        return None

    def get_name(self, bs4_page):
        """
            The function returns a dictionary with a first name and a second name. If there is no
            either first or second name, this part will be returned as None.
            :param bs4_page: a BeautifulSoup object
            :return dict: a dictionary like
                    {
                        'first': 'Vladyslav',
                        'last': 'Pedosyuk'
                    }
        """
        names = {
            "first": None,
            "last": None
        }

        return names

    def get_phone(self, bs4_page):

        return None

    def get_address(self, bs4_page):
        """
            The function returns a dictionary with a line 1 and a line 2 of the address. If there is no
            either first or second name or both, this part will be returned as None.
            :param bs4_page: a BeautifulSoup object
            :return dict: a dictionary like
                    {
                        'ln1': 'Vladyslav',
                        'ln2': 'Pedosyuk'
                    }
        """
        address = {
            'ln1': None,
            'ln2': None
        }

        return address

    def get_city(self, bs4_page):

        return None

    def get_zipcode(self, bs4_page):

        return None

    def get_state(self, bs4_page):

        return None

    def get_country(self, bs4_page):

        return None

    def get_industry(self, bs4_page):

        return None

    def get_website(self, bs4_page):

        return None

    def get_emails(self, website):
        """
        This function accepts a link to a professional website, finds all links on this page,
        then tries to check subpages on emails and return them as one string delimited by commas.
        :param website: accepts a link like 'http://website.com'
        :return string: like 'vlad@email.com, vlad1@mail.net'
        """

        def find_subpages(webpage):
            """
            It parses a main page and returns all found links. This function may take much time to complete.
            :param webpage: accepts a link like 'http://website.com'
            :return list: like
                    ['http://website.com', 'http://website.com/markets',
                    'http://website.com/contact', 'http://website.com/terms']
            """

            # check if there is some info, if there is no any, just return None.
            if not webpage or not re.match(r'^http(?!http).*://.+\..+', webpage):
                return None

            # try to connect, if can't, just return None.
            try:
                r = requests.get(webpage, headers={"Accept-Language": "en-US"}, timeout=15)
                r.encoding = 'utf-8'
            except (requests.exceptions.ConnectionError, requests.exceptions.InvalidURL,
                    requests.exceptions.TooManyRedirects, requests.exceptions.MissingSchema,
                    requests.exceptions.ReadTimeout, requests.exceptions.ContentDecodingError,
                    UnicodeError) as error:
                self._logger.warning(error)
                return None
            main_page = BeautifulSoup(r.text, 'lxml')

            # add the main page also for further checking
            # the links will end up here
            cleansed = [webpage]

            def clean_links(dirty_link):
                """
                Sometimes links can be stored as a part of a correct link like only 'index.php?go=contact'
                instead of a correct 'http://website.com/index.php?go=careers'
                so we just try to concatenate known parts and return a result
                :param dirty_link: a string like 'index.php?go=contact'
                :return string: a string like 'http://website.com/index.php?go=contact' or None
                """
                if re.search(r'(http)|(www)', dirty_link) and not re.search(r'@|(mailto)', dirty_link):
                    clean_link = dirty_link
                    # check if there is a 'contact' word and proceed only if it's exist
                    # because there could be a lot of trash links
                    if re.search(r'contact', clean_link):
                        return clean_link
                    else:
                        return None
                elif re.search(r'contact', dirty_link) and not re.search(r'@|(mailto)', dirty_link):
                    # check if there is a slash at the end of a webpage end
                    # or a slash at the beginning of a link part
                    if re.match(r'.*//.*/.*', webpage) or re.match(r'/.*', dirty_link):
                        return '{0}{1}'.format(webpage, dirty_link)
                    else:
                        # if there is not, let's add it
                        return '{0}/{1}'.format(webpage, dirty_link)
                else:
                    return None

            for tag in main_page.find_all('a'):
                # use here dict.get() in order to avoid KeyError if 'href' is not existed
                if tag.get('href', None):
                    clean = clean_links(tag['href'])
                    if clean:
                        cleansed.append(clean)

            # return it pre-checked on duplicates using set
            return list(set(cleansed))

        def scrape_emails(subpage):

            if not subpage:
                return None
            try:
                r = requests.get(subpage, headers={"Accept-Language": "en-US"}, timeout=15)
            except (requests.exceptions.ConnectionError, requests.exceptions.InvalidURL,
                    requests.exceptions.MissingSchema, requests.exceptions.TooManyRedirects,
                    requests.exceptions.ReadTimeout, requests.exceptions.ContentDecodingError,
                    UnicodeError) as error:
                self._logger.warning(error)
                return None
            r.encoding = 'utf-8'
            subpage = BeautifulSoup(r.text, 'lxml')

            emails_list = []
            for tag in subpage.find_all('a'):
                # use here dict.get() in order to avoid KeyError if 'href' is not existed
                if tag.get('href', None):
                    found_object = re.search(
                        r':*([a-z0-9.\-+_]+@[a-z0-9.\-+_]+\.(?!(png|jpg|svf|gif|jpeg|tiff|bmp))[a-z]+)',
                        tag['href'], re.I)
                    if found_object:
                        emails_list.append(found_object.group(1))
            return emails_list

        def unite_emails(email_list):
            united_emails = []
            for ls in email_list:
                if ls:
                    # delete duplicates from emails scraped from a page
                    united_emails.extend(list(set(ls)))
            # delete duplicates in the resulting list of scraped emails from web-pages
            return ', '.join(list(set(united_emails)))

        if website:
            subpages = find_subpages(website)
            if subpages:
                emails = [scrape_emails(sub) for sub in subpages]
                return unite_emails(emails)
        else:
            return None

    def parse(self, link):
        """
        The function returns a dictionary of all data scraped from a houzz page
        :param link: a link to houzz professional
        :return dict: a dict like:
        {
            'link': 'http://houzz.com/professional/vlad',
            'company': 'VladPedosyuk Inc.',
            'first_name': 'Vladyslav',
            'last_name': 'Pedosyuk',
            'phone': '(608) 981-2414',
            'address_ln1': '176 Grouse Ct',
            'address_ln2': 'app.24',
            'city': 'Brownsville',
            'zipcode': '53920',
            'state': 'Wisconsin',
            'country': 'United States',
            'industry': 'Architects & Building Designers',
            'website': 'http://vlad.com',
            'emails': 'vlad1@mail.net, vlad2@gmail.com'
        }

        """

        user_agent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36'
        language = 'en-US'

        headers = {
            "Accept-Language": language,
            "User-Agent": user_agent
        }
        s = requests.session()
        r = s.get(link, headers=headers)
        r.encoding = 'utf-8'
        soup = BeautifulSoup(r.text, 'lxml')

        # get a website, name and address separately from the whole process
        website = self.get_website(soup)
        name = self.get_name(soup)
        address = self.get_address(soup)
        result = {
            'link': link,
            'company': self.get_company(soup),
            'first_name': name['first'],
            'last_name': name['last'],
            'phone': self.get_phone(soup),
            'address_ln1': address['ln1'],
            'address_ln2': address['ln2'],
            'city': self.get_city(soup),
            'zipcode': self.get_zipcode(soup),
            'state': self.get_state(soup),
            'country': self.get_country(soup),
            'industry': self.get_industry(soup),
            'website': website,
            'emails': self.get_emails(website)
        }

        return result

    def start(self):
        # THE MAIN PROCESS
        self._logger.info('Starting...')
        if self._output.ready():
            self._links = self.get_links()
        if self._links:
            # using a fancy progress bar tqdm
            bar = tqdm(self._links, unit='link', dynamic_ncols=True)
            bar.set_description('Parsing and inserting data')
            for LINK in bar:
                self._logger.debug('\nProcessing {}'.format(LINK))

                # parse a LINK and get a dictionary
                parsed_data = self.parse(LINK)

                # send it to the output instance
                self._output.append(parsed_data)

                # sendgrid integration
                if self._params['sendgrid']['enabled'] == 'yes':
                    if parsed_data.get('email', None):
                        for email in parsed_data['email'].split(', '):
                            sendgrid_status = sendgrid_email(token=self._params['sendgrid']['token'],
                                                             input_email=email,
                                                             input_class=self.__name__,
                                                             input_data=parsed_data)
                            self._logger.debug('SendGrid: {}. Status: {}'.format(email, sendgrid_status))
                    else:
                        self._logger.debug('SendGrid: {}'.format('skipped'))

                # intercom integration
                if self._params['intercom']['enabled'] == 'yes':
                    if parsed_data.get('email', None):
                        for email in parsed_data['email'].split(', '):
                            intercom_status = intercom(token=self._params['intercom']['token'],
                                                       input_email=email,
                                                       input_data=parsed_data)
                            self._logger.debug('Intercom: {}. Status: {}'.format(email, intercom_status))
                    else:
                        self._logger.debug('Intercom: {}'.format('skipped'))

            self._logger.info('Successfully processed {} links'.format(len(bar)))
            # print('Successfully processed {} links'.format(len(bar)))

    def stop(self):
        self._output.close()
        self._logger.info('Connection to the output instance closed.')

    def init_output(self):
        if self._userconf.output == 'google_sheet':
            return GoogleSheet(self._params)
        elif self._userconf.output == 'postgres':
            return Postgres(self._params)
        elif self._userconf.output == 'csv':
            return FileCSV(self._params)

    def get_links(self):
        """
        This function must be overwritten in a subclass for a specific website like Houzz.com.
        :return list: a list of professionals links without duplicates
        """
        return None
