## Postgres
This setting defines credentials to the PostgreSQL DB you are going to use. You can leave it empty if you don't want to use Postgres.
#### host
Domain or an IP-address like ```localhost``` or ```192.168.1.1```.
#### dbname
Name of the database you're going to work with like ```postgres```.
#### user
Username we will use to access the data base like ```postgres```.
#### password
Password to authorize the user we've added in the above setting like ```password123```.
#### port
Port which the Postgres DB listens to, usually it's ```5432```.
***