# Professions
This setting defines a list of profession types you can use for scraping. 

You can freely add here any entry you want. In order to do that properly, for example, for Houzz, you should go to [Houzz.com](https://www.houzz.com/professionals/) then select any desired
profession and check its link. 

For example, we want to add ```Tile, Stone and Countertop Manufacturers and Showrooms```: go to ```https://www.houzz.com/professionals/tile-stone-and-countertop``` and take a look at this link. As you see, there is a part, which is responsible for this profession - ```tile-stone-and-countertop```. Thus, the setting will look:
```"professions": ["architect", ..., "tile-stone-and-countertop"]```
