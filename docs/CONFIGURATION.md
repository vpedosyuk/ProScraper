## Configuration
Mostly, all parameters and resources are being configured in ```config/config.json``` file. Thus, we will consider this file first. All parameters need to be typed in a *json* format.

What we can configure:
* [Postgres](docs/POSTGRES.md)
* [Google Sheet](docs/GSHEET.md)
* [CSV file](docs/CSV.md)
* [Log config](docs/LOG.md)
* [SendGrid](docs/SENDGRID.md)
* [Intercom](docs/INTERCOM.md)
* [Professions](docs/PROFESSIONS.md)