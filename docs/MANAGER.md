## Manager/Worker
If you want to run several requests simultaneously, then you may consider using the **Manager/Worker** function. Basically, it checks every ```--seconds``` a Control sheet defined in the appropriate [section](docs/GSHEET.md) of ```config.json```.

Format is the following:
![Format](format.jpg "Format of the sheet")

As you see, fields here are basically parameters you would specify if you run it directly from a console. 

However, _first_, you should fill up blue fields, _second_, you should check blue fields and if they are fine, type ```yes``` in _Ready for processing_ field and after that just wait a little bit and **ProScraper** will work on your commands.