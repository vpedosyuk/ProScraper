# Intercom
Intercom integration provides a leads creation with scraped information. It's needed to specify only two parameters:
## Enabled
```yes``` or ```no```
## Token
Get an API token on [intercom.com](http://intercom.com).