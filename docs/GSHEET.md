## Google Sheet
You can leave it empty if you don't want to use Google Sheets. If you decide to use Google Sheets as your output type, then take into account that the script calls Sheets API very often, it may require purchasing a developer account with an increased amount of available API calls per day.

There are two spreadsheets the ProScraper can work with:
1. _Output_: it's used for resulting information such as leads their email, etc
2. _Control Sheet_: it's used by a [Manager](docs/MANAGER.md) instance 

### service_key
It's needed to generate ```Service account key``` on [Google Developers](https://console.developers.google.com/) previously registering your project with Sheets API. After you get this file, you can type in an absolute or relative path to this file. Example:

```"service_key: "config/service_key.json"``` 

or 

```"service_key: "/home/user1/service_key.json"```
### 1. out_spreadsheet
This is related to the first type - _Output_.
#### "name" 
Name of the file on Google Sheets. Please make sure you've allowed to access this file to the email you've generated in ```Service account key```. You can find that email in your downloaded json ```Service account key```.
#### "worksheet_title"
Name of the worksheet the script should look for in the above ```"spreadsheet"```.
***
### 2. control_spreadsheet
This is related to the second type - _Control Sheet_.
#### "name" 
Name of the file on Google Sheets. Please make sure you've allowed to access this file to the email you've generated in ```Service account key```. You can find that email in your downloaded json ```Service account key```.
#### "worksheet_title"
Name of the worksheet the script should look for in the above ```"spreadsheet"```.
***
