ver. 0.2:
* added Zillow.com support
* added Manager/Worker implementation
* added SendGrid integration
* added Intercom integration
* added Logentries integration
* configured for Heroku deployment