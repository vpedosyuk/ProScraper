# Sendgrid
Sendgrid integration provides a full checking and structure configuration during scraping. It's needed to specify only two parameters:
## Enabled
```yes``` or ```no```
## Token
Get an API token on [sendgrid.com](http://sendgrid.com).