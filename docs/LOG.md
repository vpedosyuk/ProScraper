# Log
There are three types of possible logging:
* Stream: this type is used to stream log info in your console
* File: logs in a file
* Logentries: translates to your Logentries account
##Common parameters:
#### Enabled
```yes``` or ```no```
#### Level
You can select any of ```info```, ```debug```, ```warning```, ```critical```.
##Specific parameters:
### File
#### Name
Define any name you want
### Logentries
#### Token
It's needed to create a log instance on [logentries.com](http://logentries.com) and then get a token of the newly created log.